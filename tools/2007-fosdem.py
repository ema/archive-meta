#!/usr/bin/env python3

import requests
from bs4 import BeautifulSoup

from utils.objects import Event
from utils.yaml import yaml_dump


def scrape():
    r = requests.get('https://video.debian.net/2007/fosdem/')

    soup = BeautifulSoup(r.content, 'html.parser')
    for table in soup.find_all('table'):
        for row in table.find_all('tr'):
            event = Event(speakers=[], slides=[], alt_formats={})
            cells = list(enumerate(row.find_all('td')))
            for column, cell in cells:
                if column == 0:
                    event.title = cell.get_text()
                if column == 1:
                    event.speakers = [
                        name.strip() for
                        name in cell.get_text().split(' and ')]
                if column in (2, 3, 4):
                    for link in cell.find_all('a'):
                        href = link['href']
                        if href.startswith('xvid'):
                            event.video = href
                            continue
                        if href.startswith('slides'):
                            event.slides.append(href)
                            continue
                        if href.startswith('ogg_theora/360x288'):
                            format_ = 'ogg-low'
                        elif href.startswith('ogg_theora/720x576'):
                            format_ = 'ogg-high'
                        event.alt_formats[format_] = href
            yield event


def main():
    events = list(scrape())

    with open('scraped.yml', 'w') as f:
        yaml_dump(events, f)


if __name__ == '__main__':
    main()
