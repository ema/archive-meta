#!/usr/bin/env python3

# URLs to XML for DebConf 7..13
# http://penta.debconf.org/dc7_schedule/schedule.en.xml

import argparse
import re
from datetime import datetime
from pathlib import Path
from urllib.parse import urljoin
from xml.etree import ElementTree

import dateutil.parser
import requests

from utils.files import dc11_filename, files_with_prefix, penta_filename
from utils.objects import Conference, Event, Meta, VideoFormat
from utils.pentabarf import parse_time, parse_timedelta
from utils.yaml import yaml_dump


def scrape(url, args):
    r = requests.get(url, verify=args.verify)
    tree = ElementTree.fromstring(r.content)

    conference = scrape_conference(url, tree.find('conference'), args)
    videos = list(scrape_videos(tree, conference, args))
    videos.sort(key=lambda event: event.start)
    return Meta(
        conference=conference,
        videos=videos,
    )


def index_files(conference):
    video_prefix = conference.video_base[56:] + 'high/'
    index = {}
    for fn in files_with_prefix(video_prefix):
        name = Path(fn).stem
        title = name.split('_', 1)[1].lower()
        index[title] = name
    return index


def dc13_filename(id_):
    """debconf13's filenames are named after the event ID"""
    for filename in files_with_prefix(
            '2013/debconf13/archival/{}_'.format(id_)):
        return Path(filename).stem
    return None


def summit_filename(conference, title):
    # Copied from veyepar's dj/main/unique_slugify.py
    fname = ''.join(
            [c for c in title if c.isalpha() or c.isdigit() or (c in ' _')])
    fname = fname.replace(' ', '_')
    fname = '_'.join([w for w in fname.split('_') if w])

    if conference.series == 'DebConf':
        if conference.edition == 14:
            base = '2014/debconf14/webm/{}.webm'
        if conference.edition == 15:
            base = '2015/debconf15/{}.webm'

    for filename in files_with_prefix(base.format(fname)):
        return Path(filename).stem
    return None


def scrape_videos(tree, conference, args):
    seen = set()
    for day in tree.iter('day'):
        date = dateutil.parser.parse(day.attrib['date']).date()
        print('Date:', date)

        for penta_event in day.iter('event'):
            id_ = penta_event.attrib['id']
            if id_ in seen:
                continue
            seen.add(id_)
            title = penta_event.findtext('title')

            video = penta_filename(id_)
            if conference.series == 'DebConf':
                if conference.edition == 11:
                    video = dc11_filename(title)
                if conference.edition == 13:
                    video = dc13_filename(id_)
                if conference.edition in (14, 15):
                    video = summit_filename(conference, title)

            if not video:
                continue

            description = [
                penta_event.findtext('subtitle').strip(),
                penta_event.findtext('abstract').strip(),
                penta_event.findtext('description').strip(),
            ]
            description = '\n\n'.join(
                section for section in description if section)
            description = description.replace('\r\n', '\n')

            details_url = penta_event.findtext('conf_url')
            if details_url:
                details_url = urljoin(conference.schedule, details_url)
            else:
                details_url = urljoin(
                    conference.schedule, 'events/{}.en.html'.format(id_))

            start = datetime.combine(
                date, parse_time(penta_event.findtext('start')))
            end = start + parse_timedelta(penta_event.findtext('duration'))
            event = Event(
                title=title,
                description=description,
                details_url=details_url,
                speakers=[
                    person.text for person in penta_event.iter('person')],
                room=penta_event.findtext('room'),
                start=start,
                end=end,
                video='high/{}.ogv'.format(video),
            )
            if conference.series == 'DebConf':
                if conference.edition < 14:
                    event.alt_formats = {
                        'low': 'low/{}.ogv'.format(video),
                    }
                if conference.edition < 9:
                    event.video = event.video[:-1] + 'g'
                    for format_, path in event.alt_formats.items():
                        event.alt_formats[format_] = path[:-1] + 'g'
                if conference.edition == 7:
                    event.alt_formats.update({
                        'mpeg-ntsc': 'ntsc-dvd/{}.mpeg'.format(video),
                        'mpeg-pal': 'pal-dvd/{}.mpeg'.format(video),
                    })
                if conference.edition == 13:
                    event.video = 'archival/{}.ogv'.format(video)
                    event.alt_formats['high'] = 'high/{}.ogv'.format(video)
                if conference.edition == 14:
                    event.video = 'webm/{}.webm'.format(video)
                if conference.edition == 15:
                    event.video = '{}.webm'.format(video)

            penta_lang = penta_event.findtext('language')
            if penta_lang:
                event.language = {
                    'English': 'eng',
                    'en': 'eng',
                    'es': 'spa',
                    'fr': 'fra',
                }[penta_lang]

            yield event


def scrape_conference(url, penta_conf, args):
    title = penta_conf.findtext('title')
    edition = int(re.search('\d+$', title).group(0))
    start = dateutil.parser.parse(penta_conf.findtext('start')).date()
    end = dateutil.parser.parse(penta_conf.findtext('end')).date()
    year = 2000 + edition
    video_formats = {}

    if edition < 14:
        video_formats['default'] = VideoFormat(
            resolution='720x576',
            bitrate='1700k',
            container='ogg',
            vcodec='theora',
            acodec='vorbis',
        )
        video_formats['low'] = VideoFormat(
            resolution='320x240',
            bitrate='300k',
            container='ogg',
            vcodec='theora',
            acodec='vorbis',
        )
    else:
        video_formats['default'] = VideoFormat(
            resolution='720x576',
            bitrate='1m',
            container='matroska',
            vcodec='vp8',
            acodec='vorbis',
        )

    if edition == 7:
        video_formats['mpeg-ntsc'] = VideoFormat(
            resolution='352x240',
            bitrate='400k',
            container='mpeg1',
            vcodec='mpeg1',
            acodec='ac3',
        )
        video_formats['mpeg-pal'] = VideoFormat(
            resolution='352x240',
            bitrate='400k',
            container='mpeg1',
            vcodec='mpeg1',
            acodec='mp2',
        )

    conf = Conference(
        title=title,
        series='DebConf',
        edition=edition,
        date=[start, end],
        website='https://debconf{}.debconf.org/'.format(edition),
        schedule=url,
        video_base='https://meetings-archive.debian.net/pub/debian-meetings/'
                   '{}/debconf{}/'.format(year, edition),
        video_formats=video_formats,
    )

    if penta_conf.findtext('city'):
        conf.location = penta_conf.findtext('city')

    return conf


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('url', help='Pentabarf XML feed URL')
    parser.add_argument('-k', '--no-verify',
                        action='store_false', dest='verify',
                        help='Disable SSL verification')
    args = parser.parse_args()

    meta = scrape(args.url, args)

    with open('scraped.yml', 'w') as f:
        yaml_dump(meta, f)


if __name__ == '__main__':
    main()
