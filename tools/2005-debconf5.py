#!/usr/bin/env python3

import re
from itertools import chain
from datetime import date, datetime

import Levenshtein
import dateutil.parser
import requests
from bs4 import BeautifulSoup

from utils.objects import Event
from utils.files import files_with_prefix
from utils.yaml import yaml_dump


DEBCAMP_EVENT_RE = re.compile(
    r'^(?P<start>[0-9:]{5})-(?P<end>[0-9:]{5}) "(?P<title>.*)" \(\d+\)$',
)


def debian_day():
    r = requests.get(
        'https://debconf5.debconf.org/about/debianday.html')
    soup = BeautifulSoup(r.content, 'html.parser')
    dday = date(2005, 7, 9)
    for dt in soup.find_all('dt'):
        line = dt.string
        m = DEBCAMP_EVENT_RE.match(line)
        if not m:
            print('Not an event', line)
            continue
        start = dateutil.parser.parse(m.group('start')).time()
        end = dateutil.parser.parse(m.group('end')).time()
        event = Event(
            title=m.group('title'),
            speakers=[],
            start=datetime.combine(dday, start),
            end=datetime.combine(dday, end),
            language='eng',
            video='XXX',
            allow_guesses=True,
        )
        for element in dt.next_siblings:
            if element.name == 'dd':
                author = element.get_text()
                if author.startswith('by '):
                    author = author[3:]
                if ' and ' in author:
                    authors = author.partition(' and ')
                    event.speakers.append(authors[0])
                    event.speakers.append(authors[2])
                else:
                    event.speakers.append(author)
        yield event


def debconf():
    r = requests.get(
        'https://debconf5.debconf.org/about/schedule.html')
    soup = BeautifulSoup(r.content, 'html.parser')
    table = soup.find('table', cellpadding=5)

    current_date = None
    rowspan = 0
    times = []
    for row in table.find_all('tr'):
        column = 0
        if rowspan:
            column += 1
        else:
            times = []
        event = Event(
            title='XXX',
            speakers=[],
            language='eng',
            video='XXX',
            allow_guesses=True,
        )

        for cell in row.find_all('td'):
            if cell.attrs.get('colspan') == '3':
                if cell.b:
                    # Day change
                    current_date = dateutil.parser.parse(cell.b.string).date()
                    print('Day change:', current_date)
                break

            if cell.attrs.get('rowspan'):
                # Concurrent slots
                rowspan = int(cell['rowspan'])

            if column == 0:
                if cell.string == 'Time':
                    break
                times = [
                    parse_time(timestamp.string)
                    for timestamp in cell.find_all('b')]

            event.start = datetime.combine(current_date, times[0])
            event.end = datetime.combine(current_date, times[1])

            if column == 1:
                if cell.string in (
                        'breakfast', 'Lunch', 'Dinner', 'Formal Dinner'):
                    break

                event.title = cell.b.get_text().strip()
                event.speakers = [
                    speaker.strip()
                    for speaker in cell.i.get_text().split(',')]

            if column == 2:
                event.room = cell.get_text()

            column += 1
        else:
            yield event

        if rowspan:
            rowspan -= 1


def find_video(meta):
    "Can we find a video for meta? If so, insert it"
    files = []
    title = meta.title
    for fn in files_with_prefix('2005/debconf5/ogg_theora/720x576', '.ogg'):
        fn_title = fn.split('/')[-1]
        fn_title = fn_title.rsplit('-')[1]
        fn_title = fn_title.replace('-', ' ')
        distance = Levenshtein.distance(title, fn_title)
        files.append((distance, fn_title, fn))

    files.sort()
    meta.video = files[0][2]
    meta.guesses = [file_[2] for file_ in files[1:3]]


def parse_time(time_string):
    if time_string.startswith('24:'):
        time_string = '00:' + time_string[3:]
    return dateutil.parser.parse(time_string).time()


def main():
    events = []
    for event in chain(debian_day(), debconf()):
        find_video(event)
        events.append(event)

    with open('scraped.yml', 'w') as f:
        yaml_dump(events, f)


if __name__ == '__main__':
    main()
