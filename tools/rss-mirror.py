#!/usr/bin/env python3

"""

Export video metadata as Media RSS, see
<URL: https://www.rssboard.org/media-rss > for format description.
"""


import argparse
from datetime import date
from email import utils
import os.path
from pathlib import Path
import time

import json
from lxml.etree import tostring
from lxml.builder import E
import yaml

from utils.files import metadata_files


def json_serialize(obj):
    """Serialize obj to JSON"""
    if isinstance(obj, date):
        return obj.isoformat()
    raise TypeError(
        'Object of type {} is not JSON serializable'.format(type(obj)))

def datetime2rfc822(d):
    nowtuple = d.timetuple()
    nowtimestamp = time.mktime(nowtuple)
    return utils.formatdate(nowtimestamp)

def filename2mimetype(name):
    ext = os.path.splitext(name)[1].lower()
    if '.webm' == ext:
        return 'video/webm'
    elif '.mp4' == ext:
        return 'video/mp4'
    elif '.mpeg' == ext:
        return 'video/mpeg'
    elif '.avi' == ext:
        return 'video/x-msvideo'
    elif '.ogg' == ext or  '.ogv' == ext:
        return 'video/ogg'
    else:
        print("Unknown file extention", ext, "for", name)
        return name

def rss_output(path, data):
#    print("File ", path)
#    print(json.dumps(data, default=json_serialize, indent=4))
    rssdata = E.rss(version="2.0")
    videobase = data['conference']['video_base']
    c = E.channel(
        E.title(data['conference']['title']),
    )
    if 'website' in data['conference']:
        c.append(E.link(data['conference']['website']))

    l = s = None
    if 'series' in data['conference']:
        s = data['conference']['series']
    if 'location' in data['conference']:
        l = data['conference']['location']
    if s and l:
        desc = s + ',   ' + l
    elif s:
        desc = s
    elif l:
        desc = l
    if desc:
        c.append(E.description(desc))
    for video in data['videos']:
        v = E.item(
            E.title(video['title']),
        )
        if 'description' in video:
            v.append(E.description(video['description']))
        if 'details_url' in video:
            v.append(E.link(video['details_url']))
        if 'start' in video:
            v.append(E.pubDate(datetime2rfc822(video['start'])))
        mimetype = filename2mimetype(video['video'])
        v.append(
            E.enclosure(url=videobase + video['video'],
#                        length='bytelength',
                        type=mimetype,
                       ),
        )
        c.append(v)
    rssdata.append(c)
    with path.open('wb') as f:
        f.write(tostring(
            rssdata,
            pretty_print=True,
            xml_declaration=True,
            encoding='UTF-8')
        )


def main():
    p = argparse.ArgumentParser(
        'Build RSS XML equivalents of all YAML documents under ROOT')
    p.add_argument('root', type=Path,
                   help='Base directory to operate on')
    args = p.parse_args()

    files = []
    for path in metadata_files(args.root):
        with path.open() as f:
            data = yaml.safe_load(f)
        rss_path = path.with_suffix('.rss')
        rss_output(rss_path, data)
        files.append(rss_path)

if __name__ == '__main__':
    main()
