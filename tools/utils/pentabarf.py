from datetime import time, timedelta


def parse_time(string):
    parts = string.split(':')
    parts = [int(part) for part in parts]
    assert len(parts) == 2
    return time(hour=parts[0], minute=parts[1])


def parse_timedelta(string):
    parts = string.split(':')
    parts = [int(part) for part in parts]
    assert len(parts) == 2
    seconds = (parts[0] * 60 + parts[1]) * 60
    return timedelta(seconds=seconds)
