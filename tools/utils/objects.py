from datetime import date, datetime


class MappingObject(object):
    def __init__(self, **kwargs):
        super().__setattr__('data', {})
        for key, value in kwargs.items():
            setattr(self, key, value)

    def __setattr__(self, key, value):
        if key not in self.keys:
            raise AttributeError('Unknown key {}'.format(key))

        type_ = self.types[key]
        if not isinstance(value, type_):
            raise AttributeError('{} must be a {}'.format(key, type_))

        self.data[key] = value

    def __getattr__(self, key):
        if key not in self.data:
            raise AttributeError('Unknown key {}'.format(key))
        return self.data[key]

    def __delattr__(self, key):
        if key not in self.data:
            raise AttributeError('Unknown key {}'.format(key))
        del self.data[key]


class List(object):
    """Validation helper, a list of X"""
    def __init__(self, list_of, max_length=None):
        self.list_of = list_of
        self.max_length = max_length

    def __instancecheck__(self, instance):
        if not isinstance(instance, list):
            return False
        for item in instance:
            if not isinstance(item, self.list_of):
                return False
        if self.max_length and len(instance) > self.max_length:
            return False
        return True

    def __repr__(self):
        return 'List({})'.format(self.list_of)


class Dict(object):
    """Validation helper, a dict of string: X"""
    def __init__(self, dict_of):
        self.dict_of = dict_of

    def __instancecheck__(self, instance):
        if not isinstance(instance, dict):
            return False
        for key, value in instance.items():
            if not isinstance(key, str):
                return False
            if not isinstance(value, self.dict_of):
                return False
        return True

    def __repr__(self):
        return 'Dict({})'.format(self.dict_of)


class VideoFormat(MappingObject):
    keys = [
      'resolution',
      'bitrate',
      'container',
      'vcodec',
      'acodec',
    ]
    types = {
      'resolution': str,
      'bitrate': str,
      'container': str,
      'vcodec': str,
      'acodec': str,
    }

    def __repr__(self):
        return 'VideoFormat({})'.format(self.title)


class Conference(MappingObject):
    keys = [
        'title',
        'series',
        'edition',
        'location',
        'date',
        'website',
        'schedule',
        'video_base',
        'video_formats',
        'comment',
    ]
    types = {
        'title': str,
        'series': str,
        'edition': (str, int),
        'location': str,
        'date': List(date, max_length=2),
        'website': str,
        'schedule': str,
        'video_base': str,
        'video_formats': Dict(VideoFormat),
        'comment': str,
    }

    def __repr__(self):
        return 'Conference({})'.format(self.title)


class Event(MappingObject):
    keys = [
        'title',
        'speakers',
        'description',
        'details_url',
        'room',
        'start',
        'end',
        'video',
        'alt_formats',
        'subtitles',
        'slides',
        'files',
        'language',
        'non-free',
    ]
    types = {
        'title': str,
        'speakers': List(str),
        'description': str,
        'details_url': str,
        'room': str,
        'start': (date, datetime),
        'end': (date, datetime),
        'video': str,
        'alt_formats': Dict(str),
        'subtitles': Dict(str),
        'slides': List(str),
        'files': List(str),
        'language': str,
        'non-free': str,
    }

    def __init__(self, allow_guesses=False, **kwargs):
        super().__init__(**kwargs)
        if allow_guesses:
            object.__setattr__(self, 'keys', self.keys + ['guesses'])

    def __repr__(self):
        return 'Event({})'.format(self.title)


class Meta(MappingObject):
    keys = [
        'conference',
        'videos',
    ]
    types = {
        'conference': Conference,
        'videos': List(Event),
    }

    def __repr__(self):
        return 'Meta({})'.format(self.conference.title)
